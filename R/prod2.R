prod2 <-
function(formula, data, con = TRUE, digits = 3) {

  if(nrow(data) == 0)
    stop("\n The data frame is empty. \n\n")

  CALL <- match.call()

  tmp <- as.data.frame(lapply(data, function(x) if(mode(x) == "character") factor(x) else x))
          
  f <- formula
  if(f[[2]] == 1) {
    tmp$V1 <- rep(1, nrow(tmp))
    f <- formula(~ V1)
    }
  
  tmp$n_avg <- (tmp$n_t1 + tmp$n_t2) / 2
  #con <- TRUE
  #con <- FALSE
  #if(!con) tmp$nbexicon <- tmp$nbentcon <- rep(0, nrow(tmp))
  tmp$nblos <- tmp$nbexidea + tmp$nbexiwit
  
  if(con) {
    
    tmp$nboff <- tmp$nbexisla + tmp$nbexisal + tmp$nbexicon + tmp$nbexigif
    tmp$nbint <- tmp$nbentpur + tmp$nbentcon + tmp$nbentgif
    
  } else {
    
    tmp$nboff <- tmp$nbexisla + tmp$nbexisal + tmp$nbexigif
    tmp$nbint <- tmp$nbentpur + tmp$nbentgif
    
    tmp$n_t1 <- tmp$n_t1 + tmp$nbentcon
    tmp$n_t2 <- tmp$n_t2 + tmp$nbexicon
    
    }
  
  tmp$nboff.net <- tmp$nboff - tmp$nbint

  tab <- zagg(formula = formula(paste("n_t1 ~ ", f[2])), data = tmp, FUN = sum) 
  names(tab)[ncol(tab)] <- "n_t1"
  tab$n_t2 <- zagg(formula = formula(paste("n_t2 ~ ", f[2])), data = tmp, FUN = sum)$x.agg 
  tab$n_avg <- zagg(formula = formula(paste("n_avg ~ ", f[2])), data = tmp, FUN = sum)$x.agg 
  tab$nboff <- zagg(formula = formula(paste("nboff ~ ", f[2])), data = tmp, FUN = sum)$x.agg 
  tab$nbint <- zagg(formula = formula(paste("nbint ~ ", f[2])), data = tmp, FUN = sum)$x.agg 
  tab$nboff.net <- zagg(formula = formula(paste("nboff.net ~ ", f[2])), data = tmp, FUN = sum)$x.agg 
  tab$delta.n <- tab$n_t2 - tab$n_t1
  tab$nbprod <- (tab$n_t2 - tab$n_t1) + tab$nboff.net

  tab2 <- zagg(formula = formula(paste("n_t1 ~ ", f[2])), data = tmp, FUN = sum) 
  tab2$amr <- tab$n_t2 / tab$n_t1
  tab2$alpha <- (tab$n_t2 - tab$n_t1) / tab$n_avg 
  tab2$hoff <- tab$nboff / tab$n_avg
  tab2$hoff.net <- tab$nboff.net / tab$n_avg
  tab2$prod <- tab$nbprod / tab$n_avg
  tab2 <- tab2[, -(match("x.agg", names(tab2)))]
  
  tab.summ <- tab2

  ## Output
      
  z <- (ncol(tab.summ) - 3):ncol(tab.summ)
  tab.summ[, z] <- round(tab.summ[, z], digits = digits)
  
  row.names(tab) <- 1:nrow(tab)
  row.names(tab.summ) <- 1:nrow(tab.summ)
  
  list(tab = tab, tab.summ = tab.summ)
  
  }
