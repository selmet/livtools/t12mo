import12mo <-
function(db = NULL, sla2dea = NULL) {
  
  # Changes from 12mo_v1 to 12mo_v2:
  #  - "T_Head" is replaced by "T_HERD"
  #  - "typesla" is replaced by "typsla"
  #  - The contents of "event" (entries and exits) has been changes from numbers to characters 

  if(!require(RODBC))
    stop("\n This function requires the RODBC package. \n\n")
  on.exit(odbcCloseAll())
  
  #------------ Data importation with RODBC
  
  if(is.null(db)) {
    require(tcltk)
    db <- tclvalue(tkgetOpenFile(filetypes = "{{MS Access database 2007} *.accdb} {{MS Access database 2003} *.mdb}"))
    }  

  ext <- substr(db, start = nchar(db) - 4, stop = nchar(db))
  if(ext == "accdb") Channel <- odbcConnectAccess2007(db) else Channel <- odbcConnectAccess(db)

  listname <- sqlTables(Channel)$TABLE_NAME
  if(!("T_HERD" %in% listname & "T_Q1" %in% listname & "T_Q2_1" %in% listname & "T_Q2_2" %in% listname)) {
    stop("\n This is not a 12MO database \n\n")
    on.exit(odbcCloseAll())
    }

  therd <- sqlFetch(channel = Channel, sqtable = "T_HERD")
  tq1 <- sqlFetch(channel = Channel, sqtable = "T_Q1")
  tq2_1 <- sqlFetch(channel = Channel, sqtable = "T_Q2_1")
  tq2_2 <- sqlFetch(channel = Channel, sqtable = "T_Q2_2")
  
  tsadmin1 <- sqlFetch(channel = Channel, sqtable = "Ts_admin1")
  tsadmin2 <- sqlFetch(channel = Channel, sqtable = "Ts_admin2")
  tsadmin3 <- sqlFetch(channel = Channel, sqtable = "Ts_admin3")
  tsbreed <- sqlFetch(channel = Channel, sqtable = "Ts_breed")
  tsfarmgroup <- sqlFetch(channel = Channel, sqtable = "Ts_farmgroup")
  tstyploc <- sqlFetch(channel = Channel, sqtable = "Ts_typloc")
    
  odbcCloseAll()
  
  # Eventual recoding of slaugthering to natural deaths
  
  #sla2dea <- c("DIS", "TRA")
  #sla2dea <- c("DI", "TRA")
  if(!is.null(sla2dea)) {
    
    test <- sla2dea %in% c("ORD", "DIS", "FEE", "TRA")
    if(length(test) > sum(test))
      stop("\n sla2dea must be within ORD/DIS/FEE/TRA. \n\n")
    
    tq2_2$event[tq2_2$typsla %in% sla2dea] <- "DEA"
  }
  
  #------------ Working tables

#tq1 <- import(db = db, tab = "T_Q1")
#tq2_1 <- import(db = db, tab = "T_Q2_1")
#tq2_2 <- import(db = db, tab = "T_Q2_2")
#head(tq1)
#head(tq2_1)
#head(tq2_2)

  tq1 <- tq1[!is.na(tq1$idfarm) & !is.na(tq1$species) & !is.na(tq1$sex) & !is.na(tq1$age), ]
  tq2_1 <- tq2_1[!is.na(tq2_1$idfarm) & !is.na(tq2_1$species) & !is.na(tq2_1$sex) & !is.na(tq2_1$age) & !is.na(tq2_1$event), ]
  tq2_2 <- tq2_2[!is.na(tq2_2$idfarm) & !is.na(tq2_2$species) & !is.na(tq2_2$sex) & !is.na(tq2_2$age) & !is.na(tq2_2$event), ]

  tq1$sex <- toupper(tq1$sex)
  tq2_1$sex <- toupper(tq2_1$sex)
  tq2_2$sex <- toupper(tq2_2$sex)
  
  tq1$sex[tq1$sex == "FALSE"] <- "F"
  tq2_1$sex[tq2_1$sex == "FALSE"] <- "F"
  tq2_2$sex[tq2_2$sex == "FALSE"] <- "F"
  
  tq1$breed <- as.character(tq1$breed) ; tq1$breed[is.na(tq1$breed)] <- "00000"
  tq2_1$breed <- as.character(tq2_1$breed) ; tq2_1$breed[is.na(tq2_1$breed)] <- "00000"
  tq2_2$breed <- as.character(tq2_2$breed) ; tq2_2$breed[is.na(tq2_2$breed)] <- "00000"

  tq1 <- as.data.frame(lapply(tq1, function(x) if(mode(x) == "character") factor(x) else x))
  tq2_1 <- as.data.frame(lapply(tq2_1, function(x) if(mode(x) == "character") factor(x) else x))
  tq2_2 <- as.data.frame(lapply(tq2_2, function(x) if(mode(x) == "character") factor(x) else x))

#tq1
#tq2_1
#tq2_2
  
  # formula must include variable "age" and be nested in "~ idfarm + species + breed + sex + age"
  f <- formula(~ idfarm + species + breed + sex + age)
  listvar <- attr(terms(f), "term.labels")
  listvar

  ### tabanim

  tabanim <- tq1
  head(tabanim)

  ### tabpar
  
  tmp <- tabanim
  tmp <- tmp[tmp$sex == "F", ]
  tmp$sex <- factor(tmp$sex, levels = "F")
  ######## TEMPORARY CORRECTION FOR PASE2
  tmp$nbpar12mo[is.na(tmp$nbpar12mo)] <- 0
  tmp$nbabor12mo[is.na(tmp$nbabor12mo)] <- 0
  ######## END
  ############### WARNING: If there are no parturition or abortion,
  ############### nbpar12mo and nbabor12mo must be set to 0 in the 12MO database
  tmp <- tmp[!is.na(tmp$nbpar12mo) & !is.na(tmp$nbabor12mo), ]
  head(tmp)
  
  x <- tmp
  x$nbabor2 <- ifelse(x$age > 0, x$nbabor12mo / 2, tmp$nbabor12mo)
  x$nbpar2 <- ifelse(x$age > 0, x$nbpar12mo / 2, tmp$nbpar12mo)
  head(x)
  tab <- zagg(formula = f, data = x, FUN = length)
  names(tab)[ncol(tab)] <- "n_t2"
  tab$nbabor2 <- zagg(formula = formula(paste("nbabor2 ~", f[2])), data = x, FUN = sum)$x.agg
  tab$nbpar2 <- zagg(formula = formula(paste("nbpar2 ~", f[2])), data = x, FUN = sum)$x.agg
  tab[1:20, ]
  tab2 <- tab
  
  x <- tmp
  x$age <- x$age - 1 
  x$nbabor1 <- ifelse(x$age >= 0, x$nbabor12mo / 2, 0)
  x$nbpar1 <- ifelse(x$age >= 0, x$nbpar12mo / 2, 0)
  head(x)
  tab <- zagg(formula = f, data = x, FUN = length)
  names(tab)[ncol(tab)] <- "n_t1"
  tab$nbabor1 <- zagg(formula = formula(paste("nbabor1 ~", f[2])), data = x, FUN = sum)$x.agg
  tab$nbpar1 <- zagg(formula = formula(paste("nbpar1 ~", f[2])), data = x, FUN = sum)$x.agg
  tab[1:20, ]
  tab1 <- tab
  
  #nrow(tmp[tmp$age >= 4, ])
  #sum(tab2$n_t2[tab2$age >= 4])
  #sum(tab1$n_t1[tab1$age >= 4])
  #(sum(tab1$n_t1[tab1$age >= 4]) + sum(tab2$n_t2[tab2$age >= 4])) / 2 
  
  tab <- merge(tab1, tab2, by = listvar, all = TRUE)
  tab[1:20, ]
  tab <- tab[tab$age >= 0, ]
  tab[1:20, ]
  tab[is.na(tab)] <- 0
  tab$nbabor <- tab$nbabor1 + tab$nbabor2
  tab$nbpar <- tab$nbpar1 + tab$nbpar2
  tab[1:20, ]
  tab <- tab[, c(listvar, "n_t1", "n_t2", "nbabor", "nbpar")]
  tab[1:20, ]
  
  tabpar <- tab
  
  ### tabmov

  ## Animals present at t (= n_t2)
  
  tmp <- tq1
  head(tmp)
  tmp <- zagg(formula = f, data = tmp, FUN = length)
  names(tmp)[ncol(tmp)] <- "n_t2"
  tmp1 <- tmp
  head(tmp1)

  ## Entries in (t - 1, t)
  
  # PUR = PURCHASE, BARTER  
  # ARC = ARRIVAL IN LOAN/CONTRACT  
  # CBC = COMING BACK OF LOAN/CONTRACT	
  # GIF = GIFT, INHERITANCE, DOWRY, etc.	

  tmp <- tq2_1
  indic <- 0
  if(nrow(tmp) > 0) {
    tmp$nbentpur <- ifelse(tmp$event == "PUR", 1, 0)
    tmp$nbentcon <- ifelse(tmp$event == "ARC" | tmp$event == "CBC", 1, 0)
    tmp$nbentgif <- ifelse(tmp$event == "GIF", 1, 0)
    res <- zagg(formula = formula(paste("nbentpur ~", f[2])), data = tmp, FUN = sum)
    names(res)[ncol(res)] <- "nbentpur"  
    res$nbentcon <- zagg(formula = formula(paste("nbentcon ~", f[2])), data = tmp, FUN = sum)$x.agg
    res$nbentgif <- zagg(formula = formula(paste("nbentgif ~", f[2])), data = tmp, FUN = sum)$x.agg
  } else {
    indic <- 1
    res <- data.frame(idfarm = 1, species = NA, breed = NA, sex = NA, age = NA,
      nbentpur = 0, nbentcon = 0, nbentgif = 0, indic = 1)
  }
  res
  
  tmp2_1 <- res
  head(tmp2_1)

  ## Exits in (t - 1, t)
  
  # DEA = NATURAL DEATH (ALL DEATH OTHER THAN SLAUGHTERING)
  # SLA = SLAUGHTERING (ORD. + EMER.)
  # SAL = SALE, BARTER (LIVING ANIMALS)
  # DPC = DEPARTURE IN LOAN/CONTRACT
  # SBC = SENDING BACK LOAN/CONTRACT
  # GIF = GIFT, DOWRY, etc.
  # WIT = WITHDRAWAL, THEFT, etc.

  tmp <- tq2_2
  tmp$nbexidea <- ifelse(tmp$event == "DEA", 1, 0)
  tmp$nbexisla <- ifelse(tmp$event == "SLA", 1, 0)
  tmp$nbexisal <- ifelse(tmp$event == "SAL", 1, 0)
  tmp$nbexicon <- ifelse(tmp$event == "DPC" | tmp$event == "SBC", 1, 0)
  tmp$nbexigif <- ifelse(tmp$event == "GIF", 1, 0)
  tmp$nbexiwit <- ifelse(tmp$event == "WIT", 1, 0)
  res <- zagg(formula = formula(paste("nbexidea ~", f[2])), data = tmp, FUN = sum)
  names(res)[ncol(res)] <- "nbexidea"
  res$nbexisla <- zagg(formula = formula(paste("nbexisla ~", f[2])), data = tmp, FUN = sum)$x.agg
  res$nbexisal <- zagg(formula = formula(paste("nbexisal ~", f[2])), data = tmp, FUN = sum)$x.agg
  res$nbexicon <- zagg(formula = formula(paste("nbexicon ~", f[2])), data = tmp, FUN = sum)$x.agg
  res$nbexigif <- zagg(formula = formula(paste("nbexigif ~", f[2])), data = tmp, FUN = sum)$x.agg
  res$nbexiwit <- zagg(formula = formula(paste("nbexiwit ~", f[2])), data = tmp, FUN = sum)$x.agg
  tmp2_2 <- res
  head(tmp2_2)

  ## Merge tmp1 (= n_t2), tmp2_1 (= entries), tmp2_2 (= exits)
  
  dim(tmp1)
  tmp <- merge(tmp1, tmp2_1, by = listvar, all = TRUE)
  if(indic == 1) tmp <- tmp[is.na(tmp$indic), 1:(ncol(tmp) - 1)]
  head(tmp)
  dim(tmp)
  tmp <- merge(tmp, tmp2_2, by = listvar, all = TRUE)
  head(tmp)
  dim(tmp)
  tmp[is.na(tmp)] <- 0
  tmp$nbent <- tmp$nbentpur + tmp$nbentcon + tmp$nbentgif
  tmp$nbexi <- tmp$nbexidea + tmp$nbexisla + tmp$nbexisal + tmp$nbexicon + tmp$nbexigif + tmp$nbexiwit
  tabn_t2 <- tmp
  head(tabn_t2)

  ## Estimation of animals present at t - 1 (= n_t1)

  tmp1 <- tabn_t2[, c(listvar, "n_t2", "nbent", "nbexi")]
  head(tmp1)

  tmp2 <- tmp1
  tmp2$age <- tmp2$age + 1
  tmp2 <- tmp2[, -match("n_t2", names(tmp2))]
  head(tmp2)

  tmp <- merge(tmp1, tmp2, by = listvar, all = TRUE)
  head(tmp)
  # Force the name (to prevent error if R-automatic-naming would change)
  names(tmp)[(ncol(tmp) - 3):ncol(tmp)] <- c("nbent.x", "nbexi.x", "nbent.y", "nbexi.y")
  head(tmp)
  tmp[is.na(tmp)] <- 0
  head(tmp)
  tmp$nbent <- (tmp$nbent.x + tmp$nbent.y) / 2
  tmp$nbexi <- (tmp$nbexi.x + tmp$nbexi.y) / 2
  tmp$n_t1 <- tmp$n_t2 + tmp$nbexi  - tmp$nbent
  head(tmp)
  tmp <- tmp[tmp$age > 0,  c(listvar, "n_t1")]
  head(tmp)
  tmp$age <- tmp$age - 1
  tabn_t1 <- tmp
  head(tabn_t1)

  ## Merge tabn_t1 and tabn_t2
  
  tmp <- merge(tabn_t1, tabn_t2, by = listvar, all = TRUE)
  head(tmp)
  tmp[is.na(tmp)] <- 0
  tabmov <- tmp
  head(tabmov)

  ### tabprod
  
  z <- listvar[-match("age", listvar)]
  ff <- formula(paste("~", paste(z, collapse = " + ")))
  
  tmp <- tabmov
  tmp$b_approx <- ifelse(
    tmp$age == 0,
    tmp$n_t2 + tmp$nbexi / 2 - tmp$nbent / 2,
    0
    )
  head(tmp)

  tab <- zagg(formula = formula(paste("n_t1 ~", ff[2])), data = tmp, FUN = sum)
  names(tab)[ncol(tab)] <- "n_t1"
  tab$n_t2 <- zagg(formula = formula(paste("n_t2 ~", ff[2])), data = tmp, FUN = sum)$x.agg
  tab$b_approx <- zagg(formula = formula(paste("b_approx ~", ff[2])), data = tmp, FUN = sum)$x.agg
  tab$nbentpur <- zagg(formula = formula(paste("nbentpur ~", ff[2])), data = tmp, FUN = sum)$x.agg
  tab$nbentcon <- zagg(formula = formula(paste("nbentcon ~", ff[2])), data = tmp, FUN = sum)$x.agg
  tab$nbentgif <- zagg(formula = formula(paste("nbentgif ~", ff[2])), data = tmp, FUN = sum)$x.agg
  tab$nbexidea <- zagg(formula = formula(paste("nbexidea ~", ff[2])), data = tmp, FUN = sum)$x.agg
  tab$nbexisla <- zagg(formula = formula(paste("nbexisla ~", ff[2])), data = tmp, FUN = sum)$x.agg
  tab$nbexisal <- zagg(formula = formula(paste("nbexisal ~", ff[2])), data = tmp, FUN = sum)$x.agg
  tab$nbexicon <- zagg(formula = formula(paste("nbexicon ~", ff[2])), data = tmp, FUN = sum)$x.agg
  tab$nbexigif <- zagg(formula = formula(paste("nbexigif ~", ff[2])), data = tmp, FUN = sum)$x.agg    
  tab$nbexiwit <- zagg(formula = formula(paste("nbexiwit ~", ff[2])), data = tmp, FUN = sum)$x.agg
  tab$nbent <- zagg(formula = formula(paste("nbent ~", ff[2])), data = tmp, FUN = sum)$x.agg
  tab$nbexi <- zagg(formula = formula(paste("nbexi ~", ff[2])), data = tmp, FUN = sum)$x.agg
  head(tab)

  tmp <- tabanim
  tmp$b <- tmp$nbbornaliv1 + tmp$nbbornaliv2
  tmp <- zagg(formula = b ~ idfarm + species + breed, data = tmp, FUN = sum)
  names(tmp)[ncol(tmp)] <- "b"
  tmp$b <- tmp$b / 2
  nr <- nrow(tmp)
  tmp <- rbind(tmp, tmp)
  tmp$sex <- c(rep("F", nr), rep("M", nr))
  head(tmp)
  
  tab <- merge(tab, tmp, by = z, all = TRUE)
  head(tab)
  tab[is.na(tab)] <- 0
  head(tab) 
  
  tabprod <- tab[, c(z, "n_t1", "n_t2", "b", "b_approx",
    "nbexidea", "nbexisla", "nbexisal", "nbexicon", "nbexigif", "nbexiwit",
    "nbentpur", "nbentcon", "nbentgif", "nbent", "nbexi")]
  head(tabprod)

  #------------ Output

  list(therd = therd, tq1 = tq1, tq2_1 = tq2_1, tq2_2 = tq2_2,
    tabanim = tabanim, tabpar = tabpar, tabmov = tabmov, tabprod = tabprod,
    tsadmin1 = tsadmin1, tsadmin2 = tsadmin2, tsadmin3 = tsadmin3,
    tsbreed = tsbreed, tsfarmgroup = tsfarmgroup, tstyploc = tstyploc)

  }
