syntmov <-
function(formula, data, digits = 3) {

  if(nrow(data) == 0)
    stop("\n The data frame is empty. \n\n")

  CALL <- match.call()

  tmp <- as.data.frame(lapply(data, function(x) if(mode(x) == "character") factor(x) else x))
          
  f <- formula
  if(f[[2]] == 1) {
    tmp$V1 <- rep(1, nrow(tmp))
    f <- formula(~ V1)
    }
  
  tab <- exit(formula = f, data = tmp)
  z <- (ncol(tab) - 2):ncol(tab)
  tab <- tab[, -z]
  tab$hdea <- exit(formula = f, data = tmp, event = "DEA", digits = digits)$h
  tab$hoff <- exit(formula = f, data = tmp, event = "OFF", digits = digits)$h
  tab$hwit <- exit(formula = f, data = tmp, event = "WIT", digits = digits)$h
  tab$hint <- entry(formula = f, data = tmp, event = "INT", digits = digits)$h
  tab$hoff.net <- tab$hoff - tab$hint
  tab$htot.net <- tab$hdea + tab$hoff.net + tab$hwit
  tab$pdea.c <- 1 - exp(-tab$hdea)
  tab$pdea <- (tab$hdea / tab$htot.net) * (1 - exp(-tab$htot.net))
  tab$poff.net <- (tab$hoff.net / tab$htot.net) * (1 - exp(-tab$htot.net))

  ## Output
      
  tab <- tab[, -(ncol(tab) - 9)]
  z <- (ncol(tab) - 8):ncol(tab)
  tab[, z] <- round(tab[, z], digits = digits)
  row.names(tab) <- 1:nrow(tab)
  
  tab
  
  }
