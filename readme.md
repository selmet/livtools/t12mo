## Description

A package of R functions t12mo was built to facilitate the calculation of the demographic parameters from [12MO](https://gitlab.cirad.fr/selmet/livtools/12mo) data survey.

Chapters 6, 7 and 8 of the [12mo demographic survey method manual](https://gitlab.cirad.fr/selmet/livtools/12mo/-/blob/master/doc/manual_12mo_v2.2.pdf) describe the calculation of demographic parameters in detail and provide numerical examples.

## How to install

The easiest way is to download [here](https://gitlab.cirad.fr/selmet/livtools/t12mo/-/releases) the latest version of the source package from the release section of this repository.

Maybe you prefer install the package t12mo directly from the remote repository gitlab, you need to ensure that you have Git software installed on your system. More details [here](https://docs.posit.co/ide/user/ide/guide/tools/version-control.html)
 on how to install the versioning tool Git in [RStudio](https://posit.co/download/rstudio-desktop/)

Then you use install_git from the package [remotes.](https://CRAN.R-project.org/package=remotes) 

```R
install.packages("remotes")
library(remotes)
```

for the current developing version:
```R
remotes::install_git('https://gitlab.cirad.fr/selmet/livtools/t12mo')
```
or for a specific version:
```R
remotes::install_git('https://gitlab.cirad.fr/selmet/livtools/t12mo', ref = "2.2.0")
```

## Bibliography

We highly recommend to have a look to the handbook listed below describing the general theory behind calculation of the demographic parameters, in particular concepts such as probability, hazard rate, demographic interferences or competing risks:

[Lesnoff, M., Lancelot, R., Moulin, C.-H., Messad, S., Juanès, X., Sahut, C., 2014](http://link.springer.com/book/10.1007/978-94-017-9026-0). Calculation of demographic parameters in tropical livestock herds: a discrete time approach with laser animal-based monitoring data. Springer, Dordrecht.

## Support

Please send a message to the maintainer: Julia Vuattoux <julia.vuattoux@cirad.fr>

## License
[GPL (>= 3)](https://www.gnu.org/licenses/gpl-3.0.html)
